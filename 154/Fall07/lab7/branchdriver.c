#include <stdio.h>
#include <stdlib.h>
#include "branchpredictor.h"
#include "branchdriver.h"

int main(int argc, char *argv[])
{
	branch_predictor *bp;
	FILE *infile;
	char buffer[128];
	int accesses, mispreds;
	if (argc != 2)
	{
		printf("Usage: driver <filename>\n");
		exit(0);
	}

	infile = fopen(argv[1],"r");
	if (infile == NULL)
	{
		printf("Could not open file %s\n",argv[1]);
		exit(0);
	}

	// make the branch predictor
	bp = create_predictor(1024);

	accesses = 0;
	mispreds = 0;

	while (!feof(infile))
	{
		unsigned int addr, nextpc;
		short int direction;
		unsigned int pred;
		fgets(buffer, 128, infile);
		// PC
		addr = atoi(buffer);

		if (feof(infile))
			break;
		fgets(buffer, 128, infile);
		// direction
		direction = atoi(buffer);

		fgets(buffer, 128, infile);
		// nextpc
		nextpc = atoi(buffer);

		pred = predict(bp, addr);
		update(bp, addr, (nextpc != addr+4), nextpc);

		if (pred != nextpc)
		{
			mispreds++;
			printf("Miss on %d\n",accesses);
		}
		accesses++;
		
	}
	printf("%d accesses, %d mispredictions\n",accesses,mispreds);
}

