#ifndef MAIN_H
#define MAIN_H

/* allocate and initialize your predictor
 * return a pointer to the newly created predictor
 */
branch_predictor *create_predictor(int size);
/* inputs: PC of current instruction (not necessarily a branch)
 * outputs: nextPC (PC+4 if no prediction, target if taken prediction)
 */
unsigned int predict(branch_predictor *bp, unsigned int PC);

/* inputs: PC of instruction
 *	   0 or 1, depending on if there was a prediction
 *	   NextPC - PC+4 if not taken, target if taken
 * outputs: none
 */

void update(	branch_predictor *bp, 
		unsigned int PC, 
		short int predicted, 
		unsigned int NextPC);
#endif
