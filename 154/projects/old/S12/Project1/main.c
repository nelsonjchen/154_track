
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

// these are the structures used in this simulator


// these are the control signals for a single instruction
typedef struct _signals{
	int aluop;
	int mw;
	int mr;
	int mtr;
	int asrc;
	int btype;
	int rdst;
	int rw;
} Signals;

// these are the decoded fields for a single instruction
typedef struct _fields{
	int rd;
	int rs;
	int rt;
	int imm;
	int op;
	int func;
} Fields;

// this is all of the information for a single instruction
typedef struct _instinfo{
	int inst;
	Signals signals;
	Fields fields;
	int pc;
	int aluout;
	int memout;
	int sourcereg;
	int targetreg;
	int destreg;
	int destdata;
	char string[30];
	int s1data;
	int s2data;
	int input1;
	int input2;
} InstInfo;

// global variables
// register file
// instruction memory
// data memory
// program counter


// these are the different functions you need to write
int load(char *filename);
void fetch(InstInfo *);
void decode(InstInfo *);
void execute(InstInfo *);
void memory(InstInfo *);
void writeback(InstInfo *);

// this function is provided for you
void print(InstInfo *, int);

void main(int argc, char *argv[])
{
	InstInfo curInst;
	InstInfo *instPtr = &curInst;
	int instnum = 0;
	int maxpc;
	FILE *program;
	if (argc != 3)
	{
		printf("Usage: sim -file filename\n");
		exit(0);
	}

	maxpc = load(argv[2]);

	while (pc <= maxpc)
	{
		fetch(instPtr);
		decode(instPtr);
		execute(instPtr);
		memory(instPtr);
		writeback(instPtr);
		print(instPtr,instnum++);
	}
}

/* load
 *
 * Given the filename, which is a text file that 
 * contains the instructions stored as integers 
 *
 * You will need to load it into your global structure that 
 * stores all of the instructions.
 */
int load(char *filename)
{
}

/* fetch
 *
 * This fetches the next instruction and updates the program counter.
 * "fetching" means filling in the inst field of the instruction.
 */
void fetch(InstInfo *instruction)
{
}

/* decode
 *
 * This decodes an instruction.  It looks at the inst field of the 
 * instruction.  Then it decodes the fields into the fields data 
 * member.  The first one is given to you.
 *
 * Then it checks the op code.  Depending on what the opcode is, it
 * fills in all of the signals for that instruction.
 */
void decode(InstInfo *instruction)
{
	// fill in the signals and fields
	int val = inst->inst;
	int op, func;
	instruction->fields.op = (val >> 26) & 0x03f;
	// fill in the rest of the fields here

	// now fill in the signals

	// if it is an add
	{
		instruction->signals.aluop = 2;
		instruction->signals.mw = 0;
		instruction->signals.mr = 0;
		instruction->signals.mtr = 0;
		instruction->signals.asrc = 0;
		instruction->signals.btype = 0;
		instruction->signals.rdst = 1;
		instruction->signals.rw = 1;
		sprintf(instruction->string,"add $%d, $%d, $%d",
			instruction->fields.rd, instruction->fields.rs, 
			instruction->fields.rt);
		instruction->destreg = instruction->fields.rd;
	}


	// fill in s1data and input2
}

/* execute
 *
 * This fills in the aluout value into the instruction and destdata
 */

void execute(InstInfo *instruction)
{

}

/* memory
 *
 * If this is a load or a store, perform the memory operation
 */
void memory(InstInfo *instruction)
{

}

/* writeback
 *
 * If a register file is supposed to be written, write to it now
 */
void writeback(InstInfo *instruction)
{
}

/* print
 *
 * prints out the state of the simulator after each instruction
 */
void print(InstInfo *inst, int count)
{
	int i, j;
	printf("Instruction %d: %d\n",count,inst->inst);
	printf("%s\n",inst->string);
	printf("Fields: {rd: %d, rs: %d, rt: %d, imm: %d}\n",
		inst->fields.rd, inst->fields.rs, inst->fields.rt, inst->fields.imm);
	printf("Control Bits:\n{alu: %d, mw: %d, mr: %d, mtr: %d, asrc: %d, bt: %d, rdst: %d, rw: %d}\n",
		inst->signals.aluop, inst->signals.mw, inst->signals.mr, inst->signals.mtr, inst->signals.asrc,
		inst->signals.btype, inst->signals.rdst, inst->signals.rw);
	printf("ALU Result: %d\n",inst->aluout);
	if (inst->signals.mr)
		printf("Mem Result: %d\n",inst->memout);
	else
		printf("Mem Result: X\n");
	for(i=0;i<8;i++)
	{
		for(j=0;j<32;j+=8)
			printf("$%d: %4d ",i+j,regfile[i+j]);
		printf("\n");
	}
	printf("\n");
}

