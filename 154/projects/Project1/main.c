
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "functions.h"

int main(int argc, char *argv[])
{
	InstInfo curInst;
	InstInfo *instPtr = &curInst;
	int instnum = 0;
	int maxpc;
	FILE *program;
	if (argc != 2)
	{
		printf("Usage: sim filename\n");
		exit(0);
	}

	maxpc = load(argv[1]);
	printLoad(maxpc);

	while (pc <= maxpc)
	{
		fetch(instPtr);
		decode(instPtr);
		execute(instPtr);
		memory(instPtr);
		writeback(instPtr);
		print(instPtr,instnum++);
	}
	exit(0);
}


