
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "functions.h"

int main(int argc, char *argv[])
{
	int maxpc;
	if (argc != 3)
	{
		printf("Usage: sim -file filename\n");
		exit(0);
	}

	maxpc = load(argv[2]);
	printLoad(maxpc);
	exit(0);
}

