fib     equ    4000        ;address of variable fib
index   equ    4001	   ;address of variable index
two     equ    4002	   ;address of increment
array   equ    4003        ;address of array[0]
;
;	You need to initialize ac to store the value you want for fib!!
	stod   fib         ;initialize upper bound
	loco   4003
	swap		   ; initialize sp = & (array[0])
        loco   2           
	stod   two	   ; initialize two = 2
	loco   1
	stol   0	   ;initialize array[0] = 1
	stol   1	   ;initialize array[1] = 1
	stod   index       ;initialize index = 1
	subd   fib
        jpos   equal        ;if(ac > 0) /*(index >= fib)*/ then goto loop
        jzer   equal
;	update both array[0] and array[1] with the next two items in 
;	fibonacci sequence
loop    lodl   0	   ; ac = array[0]
        addl   1           ; ac = array[0] + array[1]
        stol   0           ; array[0] = ac
        addl   1           ; ac = array[0] + array[1]
        stol   1           ; array[1] = ac
	lodd   index
	addd   two
	stod   index	   ; index = index + 2
        subd   fib         ; ac = index - fib
        jneg   loop        ;if(ac < 0) /*(index < fib)*/ then goto loop
after   jzer   equal
; here if index = fib + 1
        lodl   0	; answer is array[0]
	jump   done
; here if index = fib
equal   lodl   1	; answer is array[1]
done    stop
        end
