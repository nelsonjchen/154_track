-- VHDL model created from C:\Xilinx\virtex2\data\drawing\nor8.sch - Tue May 02 16:25:53 2006


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity NOR8_MXILINX_s06_8bit_alu is
   port ( I0 : in    std_logic; 
          I1 : in    std_logic; 
          I2 : in    std_logic; 
          I3 : in    std_logic; 
          I4 : in    std_logic; 
          I5 : in    std_logic; 
          I6 : in    std_logic; 
          I7 : in    std_logic; 
          O  : out   std_logic);
end NOR8_MXILINX_s06_8bit_alu;

architecture BEHAVIORAL of NOR8_MXILINX_s06_8bit_alu is
   attribute BOX_TYPE   : STRING ;
   attribute RLOC       : STRING ;
   signal dummy   : std_logic;
   signal S0      : std_logic;
   signal S1      : std_logic;
   signal O_DUMMY : std_logic;
   component FMAP
      port ( I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : in    std_logic);
   end component;
   attribute BOX_TYPE of FMAP : COMPONENT is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : COMPONENT is "BLACK_BOX";
   
   component NOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NOR2 : COMPONENT is "BLACK_BOX";
   
   attribute RLOC of I_36_29 : LABEL is "X0Y0";
   attribute RLOC of I_36_138 : LABEL is "X0Y0";
   attribute RLOC of I_36_144 : LABEL is "X0Y1";
begin
   O <= O_DUMMY;
   I_36_29 : FMAP
      port map (I1=>I0, I2=>I1, I3=>I2, I4=>I3, O=>S0);
   
   I_36_110 : OR4
      port map (I0=>I0, I1=>I1, I2=>I2, I3=>I3, O=>S0);
   
   I_36_127 : OR4
      port map (I0=>I4, I1=>I5, I2=>I6, I3=>I7, O=>S1);
   
   I_36_138 : FMAP
      port map (I1=>I4, I2=>I5, I3=>I6, I4=>I7, O=>S1);
   
   I_36_140 : NOR2
      port map (I0=>S0, I1=>S1, O=>O_DUMMY);
   
   I_36_144 : FMAP
      port map (I1=>S0, I2=>S1, I3=>dummy, I4=>dummy, O=>O_DUMMY);
   
end BEHAVIORAL;


-- VHDL model created from C:\Xilinx\virtex2\data\drawing\m2_1.sch - Tue May 02 16:25:53 2006


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity M2_1_MXILINX_s06_8bit_alu is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          S0 : in    std_logic; 
          O  : out   std_logic);
end M2_1_MXILINX_s06_8bit_alu;

architecture BEHAVIORAL of M2_1_MXILINX_s06_8bit_alu is
   attribute BOX_TYPE   : STRING ;
   signal M0 : std_logic;
   signal M1 : std_logic;
   component AND2B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2B1 : COMPONENT is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : COMPONENT is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : COMPONENT is "BLACK_BOX";
   
begin
   I_36_7 : AND2B1
      port map (I0=>S0, I1=>D0, O=>M0);
   
   I_36_8 : OR2
      port map (I0=>M1, I1=>M0, O=>O);
   
   I_36_9 : AND2
      port map (I0=>D1, I1=>S0, O=>M1);
   
end BEHAVIORAL;


-- VHDL model created from s06_8bit_alu.sch - Tue May 02 16:25:53 2006


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity s06_8bit_alu is
   port ( A           : in    std_logic_vector (7 downto 0); 
          B           : in    std_logic_vector (7 downto 0); 
          Op          : in    std_logic_vector (2 downto 0); 
          GrandResult : out   std_logic_vector (7 downto 0); 
          Zero        : out   std_logic);
end s06_8bit_alu;

architecture BEHAVIORAL of s06_8bit_alu is
   attribute HU_SET     : STRING ;
   attribute BOX_TYPE   : STRING ;
   signal Cout0             : std_logic;
   signal Cout1             : std_logic;
   signal Cout2             : std_logic;
   signal Cout3             : std_logic;
   signal Cout4             : std_logic;
   signal Cout5             : std_logic;
   signal Cout6             : std_logic;
   signal notSLT            : std_logic;
   signal Result0           : std_logic;
   signal Result1           : std_logic;
   signal Result2           : std_logic;
   signal Result3           : std_logic;
   signal Result4           : std_logic;
   signal Result5           : std_logic;
   signal Result6           : std_logic;
   signal Result7           : std_logic;
   signal SLT               : std_logic;
   signal GrandResult_DUMMY : std_logic_vector (7 downto 0);
   component s06_1bit_alu
      port ( A      : in    std_logic; 
             B      : in    std_logic; 
             Cin    : in    std_logic; 
             Op     : in    std_logic_vector (2 downto 0); 
             Result : out   std_logic; 
             Cout   : out   std_logic);
   end component;
   
   component M2_1_MXILINX_s06_8bit_alu
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             S0 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component AND3B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3B1 : COMPONENT is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : COMPONENT is "BLACK_BOX";
   
   component NOR8_MXILINX_s06_8bit_alu
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             I5 : in    std_logic; 
             I6 : in    std_logic; 
             I7 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   attribute HU_SET of XLXI_10 : LABEL is "XLXI_10_0";
   attribute HU_SET of XLXI_11 : LABEL is "XLXI_11_1";
   attribute HU_SET of XLXI_12 : LABEL is "XLXI_12_2";
   attribute HU_SET of XLXI_13 : LABEL is "XLXI_13_3";
   attribute HU_SET of XLXI_14 : LABEL is "XLXI_14_4";
   attribute HU_SET of XLXI_15 : LABEL is "XLXI_15_5";
   attribute HU_SET of XLXI_16 : LABEL is "XLXI_16_6";
   attribute HU_SET of XLXI_17 : LABEL is "XLXI_17_7";
   attribute HU_SET of XLXI_21 : LABEL is "XLXI_21_8";
begin
   GrandResult(7 downto 0) <= GrandResult_DUMMY(7 downto 0);
   XLXI_1 : s06_1bit_alu
      port map (A=>A(0), B=>B(0), Cin=>Op(0), Op(2 downto 0)=>Op(2 downto 0),
            Cout=>Cout0, Result=>Result0);
   
   XLXI_2 : s06_1bit_alu
      port map (A=>A(1), B=>B(1), Cin=>Cout0, Op(2 downto 0)=>Op(2 downto 0),
            Cout=>Cout1, Result=>Result1);
   
   XLXI_3 : s06_1bit_alu
      port map (A=>A(2), B=>B(2), Cin=>Cout1, Op(2 downto 0)=>Op(2 downto 0),
            Cout=>Cout2, Result=>Result2);
   
   XLXI_4 : s06_1bit_alu
      port map (A=>A(3), B=>B(3), Cin=>Cout2, Op(2 downto 0)=>Op(2 downto 0),
            Cout=>Cout3, Result=>Result3);
   
   XLXI_5 : s06_1bit_alu
      port map (A=>A(4), B=>B(4), Cin=>Cout3, Op(2 downto 0)=>Op(2 downto 0),
            Cout=>Cout4, Result=>Result4);
   
   XLXI_6 : s06_1bit_alu
      port map (A=>A(5), B=>B(5), Cin=>Cout4, Op(2 downto 0)=>Op(2 downto 0),
            Cout=>Cout5, Result=>Result5);
   
   XLXI_7 : s06_1bit_alu
      port map (A=>A(6), B=>B(6), Cin=>Cout5, Op(2 downto 0)=>Op(2 downto 0),
            Cout=>Cout6, Result=>Result6);
   
   XLXI_8 : s06_1bit_alu
      port map (A=>A(7), B=>B(7), Cin=>Cout6, Op(2 downto 0)=>Op(2 downto 0),
            Cout=>open, Result=>Result7);
   
   XLXI_10 : M2_1_MXILINX_s06_8bit_alu
      port map (D0=>Result0, D1=>Result7, S0=>SLT, O=>GrandResult_DUMMY(0));
   
   XLXI_11 : M2_1_MXILINX_s06_8bit_alu
      port map (D0=>Result1, D1=>notSLT, S0=>SLT, O=>GrandResult_DUMMY(1));
   
   XLXI_12 : M2_1_MXILINX_s06_8bit_alu
      port map (D0=>Result2, D1=>notSLT, S0=>SLT, O=>GrandResult_DUMMY(2));
   
   XLXI_13 : M2_1_MXILINX_s06_8bit_alu
      port map (D0=>Result3, D1=>notSLT, S0=>SLT, O=>GrandResult_DUMMY(3));
   
   XLXI_14 : M2_1_MXILINX_s06_8bit_alu
      port map (D0=>Result4, D1=>notSLT, S0=>SLT, O=>GrandResult_DUMMY(4));
   
   XLXI_15 : M2_1_MXILINX_s06_8bit_alu
      port map (D0=>Result5, D1=>notSLT, S0=>SLT, O=>GrandResult_DUMMY(5));
   
   XLXI_16 : M2_1_MXILINX_s06_8bit_alu
      port map (D0=>Result6, D1=>notSLT, S0=>SLT, O=>GrandResult_DUMMY(6));
   
   XLXI_17 : M2_1_MXILINX_s06_8bit_alu
      port map (D0=>Result7, D1=>notSLT, S0=>SLT, O=>GrandResult_DUMMY(7));
   
   XLXI_18 : AND3B1
      port map (I0=>Op(2), I1=>Op(1), I2=>Op(0), O=>SLT);
   
   XLXI_19 : INV
      port map (I=>SLT, O=>notSLT);
   
   XLXI_21 : NOR8_MXILINX_s06_8bit_alu
      port map (I0=>GrandResult_DUMMY(7), I1=>GrandResult_DUMMY(6),
            I2=>GrandResult_DUMMY(5), I3=>GrandResult_DUMMY(4),
            I4=>GrandResult_DUMMY(3), I5=>GrandResult_DUMMY(2),
            I6=>GrandResult_DUMMY(1), I7=>GrandResult_DUMMY(0), O=>Zero);
   
end BEHAVIORAL;


