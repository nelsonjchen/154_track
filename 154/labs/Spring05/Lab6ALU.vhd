-- VHDL model created from C:\Xilinx\spartan2\data\drawing\nor8.sch - Thu May 05 14:55:15 2005


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity NOR8_MXILINX_eightbitalu is
   port ( I0 : in    std_logic; 
          I1 : in    std_logic; 
          I2 : in    std_logic; 
          I3 : in    std_logic; 
          I4 : in    std_logic; 
          I5 : in    std_logic; 
          I6 : in    std_logic; 
          I7 : in    std_logic; 
          O  : out   std_logic);
end NOR8_MXILINX_eightbitalu;

architecture BEHAVIORAL of NOR8_MXILINX_eightbitalu is
   attribute BOX_TYPE   : STRING ;
   attribute RLOC       : STRING ;
   signal dummy   : std_logic;
   signal S0      : std_logic;
   signal S1      : std_logic;
   signal O_DUMMY : std_logic;
   component FMAP
      port ( I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : in    std_logic);
   end component;
   attribute BOX_TYPE of FMAP : COMPONENT is "BLACK_BOX";
   
   component OR4
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR4 : COMPONENT is "BLACK_BOX";
   
   component NOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of NOR2 : COMPONENT is "BLACK_BOX";
   
   attribute RLOC of I_36_29 : LABEL is "R0C0.S1";
   attribute RLOC of I_36_138 : LABEL is "R0C0.S1";
   attribute RLOC of I_36_144 : LABEL is "R0C0.S0";
begin
   O <= O_DUMMY;
   I_36_29 : FMAP
      port map (I1=>I0, I2=>I1, I3=>I2, I4=>I3, O=>S0);
   
   I_36_110 : OR4
      port map (I0=>I0, I1=>I1, I2=>I2, I3=>I3, O=>S0);
   
   I_36_127 : OR4
      port map (I0=>I4, I1=>I5, I2=>I6, I3=>I7, O=>S1);
   
   I_36_138 : FMAP
      port map (I1=>I4, I2=>I5, I3=>I6, I4=>I7, O=>S1);
   
   I_36_140 : NOR2
      port map (I0=>S0, I1=>S1, O=>O_DUMMY);
   
   I_36_144 : FMAP
      port map (I1=>S0, I2=>S1, I3=>dummy, I4=>dummy, O=>O_DUMMY);
   
end BEHAVIORAL;


-- VHDL model created from C:\Xilinx\spartan2\data\drawing\m2_1e.sch - Thu May 05 14:55:15 2005


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity M2_1E_MXILINX_eightbitalu is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          E  : in    std_logic; 
          S0 : in    std_logic; 
          O  : out   std_logic);
end M2_1E_MXILINX_eightbitalu;

architecture BEHAVIORAL of M2_1E_MXILINX_eightbitalu is
   attribute BOX_TYPE   : STRING ;
   signal M0 : std_logic;
   signal M1 : std_logic;
   component AND3
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3 : COMPONENT is "BLACK_BOX";
   
   component AND3B1
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND3B1 : COMPONENT is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : COMPONENT is "BLACK_BOX";
   
begin
   I_36_30 : AND3
      port map (I0=>D1, I1=>E, I2=>S0, O=>M1);
   
   I_36_31 : AND3B1
      port map (I0=>S0, I1=>E, I2=>D0, O=>M0);
   
   I_36_38 : OR2
      port map (I0=>M1, I1=>M0, O=>O);
   
end BEHAVIORAL;


-- VHDL model created from C:\Xilinx\spartan2\data\drawing\m8_1e.sch - Thu May 05 14:55:15 2005


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity M8_1E_MXILINX_eightbitalu is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          D2 : in    std_logic; 
          D3 : in    std_logic; 
          D4 : in    std_logic; 
          D5 : in    std_logic; 
          D6 : in    std_logic; 
          D7 : in    std_logic; 
          E  : in    std_logic; 
          S0 : in    std_logic; 
          S1 : in    std_logic; 
          S2 : in    std_logic; 
          O  : out   std_logic);
end M8_1E_MXILINX_eightbitalu;

architecture BEHAVIORAL of M8_1E_MXILINX_eightbitalu is
   attribute HU_SET     : STRING ;
   attribute BOX_TYPE   : STRING ;
   signal M01 : std_logic;
   signal M03 : std_logic;
   signal M23 : std_logic;
   signal M45 : std_logic;
   signal M47 : std_logic;
   signal M67 : std_logic;
   component M2_1E_MXILINX_eightbitalu
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             E  : in    std_logic; 
             S0 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component MUXF5_L
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             S  : in    std_logic; 
             LO : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXF5_L : COMPONENT is "BLACK_BOX";
   
   component MUXF6
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             S  : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXF6 : COMPONENT is "BLACK_BOX";
   
   attribute HU_SET of I_M01 : LABEL is "I_M01_3";
   attribute HU_SET of I_M23 : LABEL is "I_M23_2";
   attribute HU_SET of I_M45 : LABEL is "I_M45_1";
   attribute HU_SET of I_M67 : LABEL is "I_M67_0";
begin
   I_M01 : M2_1E_MXILINX_eightbitalu
      port map (D0=>D0, D1=>D1, E=>E, S0=>S0, O=>M01);
   
   I_M03 : MUXF5_L
      port map (I0=>M01, I1=>M23, S=>S1, LO=>M03);
   
   I_M23 : M2_1E_MXILINX_eightbitalu
      port map (D0=>D2, D1=>D3, E=>E, S0=>S0, O=>M23);
   
   I_M45 : M2_1E_MXILINX_eightbitalu
      port map (D0=>D4, D1=>D5, E=>E, S0=>S0, O=>M45);
   
   I_M47 : MUXF5_L
      port map (I0=>M45, I1=>M67, S=>S1, LO=>M47);
   
   I_M67 : M2_1E_MXILINX_eightbitalu
      port map (D0=>D6, D1=>D7, E=>E, S0=>S0, O=>M67);
   
   I_O : MUXF6
      port map (I0=>M03, I1=>M47, S=>S2, O=>O);
   
end BEHAVIORAL;


-- VHDL model created from C:\Xilinx\spartan2\data\drawing\m2_1b1.sch - Thu May 05 14:55:15 2005


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity M2_1B1_MXILINX_eightbitalu is
   port ( D0 : in    std_logic; 
          D1 : in    std_logic; 
          S0 : in    std_logic; 
          O  : out   std_logic);
end M2_1B1_MXILINX_eightbitalu;

architecture BEHAVIORAL of M2_1B1_MXILINX_eightbitalu is
   attribute BOX_TYPE   : STRING ;
   signal M0 : std_logic;
   signal M1 : std_logic;
   component AND2B2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2B2 : COMPONENT is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : COMPONENT is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : COMPONENT is "BLACK_BOX";
   
begin
   I_36_7 : AND2B2
      port map (I0=>S0, I1=>D0, O=>M0);
   
   I_36_8 : OR2
      port map (I0=>M1, I1=>M0, O=>O);
   
   I_36_9 : AND2
      port map (I0=>D1, I1=>S0, O=>M1);
   
end BEHAVIORAL;


-- VHDL model created from C:\Xilinx\spartan2\data\drawing\add8.sch - Thu May 05 14:55:15 2005


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity ADD8_MXILINX_eightbitalu is
   port ( A   : in    std_logic_vector (7 downto 0); 
          B   : in    std_logic_vector (7 downto 0); 
          CI  : in    std_logic; 
          CO  : out   std_logic; 
          OFL : out   std_logic; 
          S   : out   std_logic_vector (7 downto 0));
end ADD8_MXILINX_eightbitalu;

architecture BEHAVIORAL of ADD8_MXILINX_eightbitalu is
   attribute BOX_TYPE   : STRING ;
   attribute RLOC       : STRING ;
   signal C0       : std_logic;
   signal C1       : std_logic;
   signal C2       : std_logic;
   signal C3       : std_logic;
   signal C4       : std_logic;
   signal C5       : std_logic;
   signal C6       : std_logic;
   signal C6O      : std_logic;
   signal dummy    : std_logic;
   signal I0       : std_logic;
   signal I1       : std_logic;
   signal I2       : std_logic;
   signal I3       : std_logic;
   signal I4       : std_logic;
   signal I5       : std_logic;
   signal I6       : std_logic;
   signal I7       : std_logic;
   signal CO_DUMMY : std_logic;
   component FMAP
      port ( I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             O  : in    std_logic);
   end component;
   attribute BOX_TYPE of FMAP : COMPONENT is "BLACK_BOX";
   
   component MUXCY_L
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             LO : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY_L : COMPONENT is "BLACK_BOX";
   
   component MUXCY
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY : COMPONENT is "BLACK_BOX";
   
   component XORCY
      port ( CI : in    std_logic; 
             LI : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XORCY : COMPONENT is "BLACK_BOX";
   
   component MUXCY_D
      port ( CI : in    std_logic; 
             DI : in    std_logic; 
             S  : in    std_logic; 
             LO : out   std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of MUXCY_D : COMPONENT is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : COMPONENT is "BLACK_BOX";
   
   attribute RLOC of I_36_16 : LABEL is "R3C0.S1";
   attribute RLOC of I_36_17 : LABEL is "R3C0.S1";
   attribute RLOC of I_36_18 : LABEL is "R2C0.S1";
   attribute RLOC of I_36_19 : LABEL is "R2C0.S1";
   attribute RLOC of I_36_20 : LABEL is "R1C0.S1";
   attribute RLOC of I_36_21 : LABEL is "R1C0.S1";
   attribute RLOC of I_36_22 : LABEL is "R0C0.S1";
   attribute RLOC of I_36_23 : LABEL is "R0C0.S1";
   attribute RLOC of I_36_55 : LABEL is "R3C0.S1";
   attribute RLOC of I_36_58 : LABEL is "R2C0.S1";
   attribute RLOC of I_36_62 : LABEL is "R2C0.S1";
   attribute RLOC of I_36_63 : LABEL is "R1C0.S1";
   attribute RLOC of I_36_64 : LABEL is "R0C0.S1";
   attribute RLOC of I_36_107 : LABEL is "R0C0.S1";
   attribute RLOC of I_36_110 : LABEL is "R1C0.S1";
   attribute RLOC of I_36_111 : LABEL is "R3C0.S1";
begin
   CO <= CO_DUMMY;
   I_36_16 : FMAP
      port map (I1=>A(0), I2=>B(0), I3=>dummy, I4=>dummy, O=>I0);
   
   I_36_17 : FMAP
      port map (I1=>A(1), I2=>B(1), I3=>dummy, I4=>dummy, O=>I1);
   
   I_36_18 : FMAP
      port map (I1=>A(2), I2=>B(2), I3=>dummy, I4=>dummy, O=>I2);
   
   I_36_19 : FMAP
      port map (I1=>A(3), I2=>B(3), I3=>dummy, I4=>dummy, O=>I3);
   
   I_36_20 : FMAP
      port map (I1=>A(4), I2=>B(4), I3=>dummy, I4=>dummy, O=>I4);
   
   I_36_21 : FMAP
      port map (I1=>A(5), I2=>B(5), I3=>dummy, I4=>dummy, O=>I5);
   
   I_36_22 : FMAP
      port map (I1=>A(6), I2=>B(6), I3=>dummy, I4=>dummy, O=>I6);
   
   I_36_23 : FMAP
      port map (I1=>A(7), I2=>B(7), I3=>dummy, I4=>dummy, O=>I7);
   
   I_36_55 : MUXCY_L
      port map (CI=>C0, DI=>A(1), S=>I1, LO=>C1);
   
   I_36_58 : MUXCY_L
      port map (CI=>C2, DI=>A(3), S=>I3, LO=>C3);
   
   I_36_62 : MUXCY_L
      port map (CI=>C1, DI=>A(2), S=>I2, LO=>C2);
   
   I_36_63 : MUXCY_L
      port map (CI=>C3, DI=>A(4), S=>I4, LO=>C4);
   
   I_36_64 : MUXCY
      port map (CI=>C6, DI=>A(7), S=>I7, O=>CO_DUMMY);
   
   I_36_73 : XORCY
      port map (CI=>CI, LI=>I0, O=>S(0));
   
   I_36_74 : XORCY
      port map (CI=>C0, LI=>I1, O=>S(1));
   
   I_36_75 : XORCY
      port map (CI=>C2, LI=>I3, O=>S(3));
   
   I_36_76 : XORCY
      port map (CI=>C1, LI=>I2, O=>S(2));
   
   I_36_77 : XORCY
      port map (CI=>C4, LI=>I5, O=>S(5));
   
   I_36_78 : XORCY
      port map (CI=>C3, LI=>I4, O=>S(4));
   
   I_36_80 : XORCY
      port map (CI=>C6, LI=>I7, O=>S(7));
   
   I_36_81 : XORCY
      port map (CI=>C5, LI=>I6, O=>S(6));
   
   I_36_107 : MUXCY_D
      port map (CI=>C5, DI=>A(6), S=>I6, LO=>C6, O=>C6O);
   
   I_36_110 : MUXCY_L
      port map (CI=>C4, DI=>A(5), S=>I5, LO=>C5);
   
   I_36_111 : MUXCY_L
      port map (CI=>CI, DI=>A(0), S=>I0, LO=>C0);
   
   I_36_221 : XOR2
      port map (I0=>A(7), I1=>B(7), O=>I7);
   
   I_36_222 : XOR2
      port map (I0=>A(6), I1=>B(6), O=>I6);
   
   I_36_223 : XOR2
      port map (I0=>A(5), I1=>B(5), O=>I5);
   
   I_36_224 : XOR2
      port map (I0=>A(4), I1=>B(4), O=>I4);
   
   I_36_225 : XOR2
      port map (I0=>A(3), I1=>B(3), O=>I3);
   
   I_36_228 : XOR2
      port map (I0=>A(0), I1=>B(0), O=>I0);
   
   I_36_229 : XOR2
      port map (I0=>A(1), I1=>B(1), O=>I1);
   
   I_36_230 : XOR2
      port map (I0=>A(2), I1=>B(2), O=>I2);
   
   I_36_239 : XOR2
      port map (I0=>C6O, I1=>CO_DUMMY, O=>OFL);
   
end BEHAVIORAL;


-- VHDL model created from eightbitalu.sch - Thu May 05 14:55:15 2005


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity eightbitalu is
   port ( A      : in    std_logic_vector (7 downto 0); 
          B      : in    std_logic_vector (7 downto 0); 
          Op     : in    std_logic_vector (2 downto 0); 
          Result : out   std_logic_vector (7 downto 0); 
          ZBit   : out   std_logic);
end eightbitalu;

architecture BEHAVIORAL of eightbitalu is
   attribute HU_SET     : STRING ;
   attribute BOX_TYPE   : STRING ;
   signal AddInputB    : std_logic_vector (7 downto 0);
   signal AddResult    : std_logic_vector (7 downto 0);
   signal AndResult    : std_logic_vector (7 downto 0);
   signal Ground       : std_logic;
   signal OrResult     : std_logic_vector (7 downto 0);
   signal OverFlow     : std_logic;
   signal Power        : std_logic;
   signal XLXN_80      : std_logic;
   signal XLXN_137     : std_logic;
   signal Result_DUMMY : std_logic_vector (7 downto 0);
   component ADD8_MXILINX_eightbitalu
      port ( A   : in    std_logic_vector (7 downto 0); 
             B   : in    std_logic_vector (7 downto 0); 
             CI  : in    std_logic; 
             CO  : out   std_logic; 
             OFL : out   std_logic; 
             S   : out   std_logic_vector (7 downto 0));
   end component;
   
   component M2_1B1_MXILINX_eightbitalu
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             S0 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : COMPONENT is "BLACK_BOX";
   
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : COMPONENT is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : COMPONENT is "BLACK_BOX";
   
   component M8_1E_MXILINX_eightbitalu
      port ( D0 : in    std_logic; 
             D1 : in    std_logic; 
             D2 : in    std_logic; 
             D3 : in    std_logic; 
             D4 : in    std_logic; 
             D5 : in    std_logic; 
             D6 : in    std_logic; 
             D7 : in    std_logic; 
             E  : in    std_logic; 
             S0 : in    std_logic; 
             S1 : in    std_logic; 
             S2 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : COMPONENT is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : COMPONENT is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : COMPONENT is "BLACK_BOX";
   
   component NOR8_MXILINX_eightbitalu
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             I2 : in    std_logic; 
             I3 : in    std_logic; 
             I4 : in    std_logic; 
             I5 : in    std_logic; 
             I6 : in    std_logic; 
             I7 : in    std_logic; 
             O  : out   std_logic);
   end component;
   
   attribute HU_SET of XLXI_1 : LABEL is "XLXI_1_4";
   attribute HU_SET of XLXI_3 : LABEL is "XLXI_3_5";
   attribute HU_SET of XLXI_4 : LABEL is "XLXI_4_6";
   attribute HU_SET of XLXI_5 : LABEL is "XLXI_5_7";
   attribute HU_SET of XLXI_6 : LABEL is "XLXI_6_8";
   attribute HU_SET of XLXI_7 : LABEL is "XLXI_7_9";
   attribute HU_SET of XLXI_8 : LABEL is "XLXI_8_10";
   attribute HU_SET of XLXI_9 : LABEL is "XLXI_9_11";
   attribute HU_SET of XLXI_10 : LABEL is "XLXI_10_12";
   attribute HU_SET of XLXI_29 : LABEL is "XLXI_29_13";
   attribute HU_SET of XLXI_30 : LABEL is "XLXI_30_14";
   attribute HU_SET of XLXI_31 : LABEL is "XLXI_31_15";
   attribute HU_SET of XLXI_32 : LABEL is "XLXI_32_16";
   attribute HU_SET of XLXI_33 : LABEL is "XLXI_33_17";
   attribute HU_SET of XLXI_34 : LABEL is "XLXI_34_18";
   attribute HU_SET of XLXI_35 : LABEL is "XLXI_35_19";
   attribute HU_SET of XLXI_36 : LABEL is "XLXI_36_20";
   attribute HU_SET of XLXI_40 : LABEL is "XLXI_40_21";
begin
   Result(7 downto 0) <= Result_DUMMY(7 downto 0);
   XLXI_1 : ADD8_MXILINX_eightbitalu
      port map (A(7 downto 0)=>A(7 downto 0), B(7 downto 0)=>AddInputB(7 downto
            0), CI=>XLXN_80, CO=>open, OFL=>OverFlow, S(7 downto
            0)=>AddResult(7 downto 0));
   
   XLXI_3 : M2_1B1_MXILINX_eightbitalu
      port map (D0=>B(0), D1=>B(0), S0=>Op(2), O=>AddInputB(0));
   
   XLXI_4 : M2_1B1_MXILINX_eightbitalu
      port map (D0=>B(1), D1=>B(1), S0=>Op(2), O=>AddInputB(1));
   
   XLXI_5 : M2_1B1_MXILINX_eightbitalu
      port map (D0=>B(2), D1=>B(2), S0=>Op(2), O=>AddInputB(2));
   
   XLXI_6 : M2_1B1_MXILINX_eightbitalu
      port map (D0=>B(3), D1=>B(3), S0=>Op(2), O=>AddInputB(3));
   
   XLXI_7 : M2_1B1_MXILINX_eightbitalu
      port map (D0=>B(4), D1=>B(4), S0=>Op(2), O=>AddInputB(4));
   
   XLXI_8 : M2_1B1_MXILINX_eightbitalu
      port map (D0=>B(5), D1=>B(5), S0=>Op(2), O=>AddInputB(5));
   
   XLXI_9 : M2_1B1_MXILINX_eightbitalu
      port map (D0=>B(6), D1=>B(6), S0=>Op(2), O=>AddInputB(6));
   
   XLXI_10 : M2_1B1_MXILINX_eightbitalu
      port map (D0=>B(7), D1=>B(7), S0=>Op(2), O=>AddInputB(7));
   
   XLXI_11 : INV
      port map (I=>Op(2), O=>XLXN_80);
   
   XLXI_12 : AND2
      port map (I0=>B(0), I1=>A(0), O=>AndResult(0));
   
   XLXI_13 : AND2
      port map (I0=>B(1), I1=>A(1), O=>AndResult(1));
   
   XLXI_14 : AND2
      port map (I0=>B(2), I1=>A(2), O=>AndResult(2));
   
   XLXI_15 : AND2
      port map (I0=>B(3), I1=>A(3), O=>AndResult(3));
   
   XLXI_16 : AND2
      port map (I0=>B(4), I1=>A(4), O=>AndResult(4));
   
   XLXI_17 : AND2
      port map (I0=>B(5), I1=>A(5), O=>AndResult(5));
   
   XLXI_18 : AND2
      port map (I0=>B(6), I1=>A(6), O=>AndResult(6));
   
   XLXI_19 : AND2
      port map (I0=>B(7), I1=>A(7), O=>AndResult(7));
   
   XLXI_20 : OR2
      port map (I0=>B(0), I1=>A(0), O=>OrResult(0));
   
   XLXI_21 : OR2
      port map (I0=>B(1), I1=>A(1), O=>OrResult(1));
   
   XLXI_22 : OR2
      port map (I0=>B(2), I1=>A(2), O=>OrResult(2));
   
   XLXI_23 : OR2
      port map (I0=>B(3), I1=>A(3), O=>OrResult(3));
   
   XLXI_24 : OR2
      port map (I0=>B(4), I1=>A(4), O=>OrResult(4));
   
   XLXI_25 : OR2
      port map (I0=>B(5), I1=>A(5), O=>OrResult(5));
   
   XLXI_26 : OR2
      port map (I0=>B(6), I1=>A(6), O=>OrResult(6));
   
   XLXI_27 : OR2
      port map (I0=>B(7), I1=>A(7), O=>OrResult(7));
   
   XLXI_29 : M8_1E_MXILINX_eightbitalu
      port map (D0=>AddResult(0), D1=>XLXN_137, D2=>Ground, D3=>Ground,
            D4=>Ground, D5=>OrResult(0), D6=>AddResult(0), D7=>AndResult(0),
            E=>Power, S0=>Op(0), S1=>Op(1), S2=>Op(2), O=>Result_DUMMY(0));
   
   XLXI_30 : M8_1E_MXILINX_eightbitalu
      port map (D0=>AddResult(1), D1=>Ground, D2=>Ground, D3=>Ground,
            D4=>Ground, D5=>OrResult(1), D6=>AddResult(1), D7=>AndResult(1),
            E=>Power, S0=>Op(0), S1=>Op(1), S2=>Op(2), O=>Result_DUMMY(1));
   
   XLXI_31 : M8_1E_MXILINX_eightbitalu
      port map (D0=>AddResult(2), D1=>Ground, D2=>Ground, D3=>Ground,
            D4=>Ground, D5=>OrResult(2), D6=>AddResult(2), D7=>AndResult(2),
            E=>Power, S0=>Op(0), S1=>Op(1), S2=>Op(2), O=>Result_DUMMY(2));
   
   XLXI_32 : M8_1E_MXILINX_eightbitalu
      port map (D0=>AddResult(5), D1=>Ground, D2=>Ground, D3=>Ground,
            D4=>Ground, D5=>OrResult(5), D6=>AddResult(5), D7=>AndResult(5),
            E=>Power, S0=>Op(0), S1=>Op(1), S2=>Op(2), O=>Result_DUMMY(5));
   
   XLXI_33 : M8_1E_MXILINX_eightbitalu
      port map (D0=>AddResult(4), D1=>Ground, D2=>Ground, D3=>Ground,
            D4=>Ground, D5=>OrResult(4), D6=>AddResult(4), D7=>AndResult(4),
            E=>Power, S0=>Op(0), S1=>Op(1), S2=>Op(2), O=>Result_DUMMY(4));
   
   XLXI_34 : M8_1E_MXILINX_eightbitalu
      port map (D0=>AddResult(3), D1=>Ground, D2=>Ground, D3=>Ground,
            D4=>Ground, D5=>OrResult(3), D6=>AddResult(3), D7=>AndResult(3),
            E=>Power, S0=>Op(0), S1=>Op(1), S2=>Op(2), O=>Result_DUMMY(3));
   
   XLXI_35 : M8_1E_MXILINX_eightbitalu
      port map (D0=>AddResult(6), D1=>Ground, D2=>Ground, D3=>Ground,
            D4=>Ground, D5=>OrResult(6), D6=>AddResult(6), D7=>AndResult(6),
            E=>Power, S0=>Op(0), S1=>Op(1), S2=>Op(2), O=>Result_DUMMY(6));
   
   XLXI_36 : M8_1E_MXILINX_eightbitalu
      port map (D0=>AddResult(7), D1=>Ground, D2=>Ground, D3=>Ground,
            D4=>Ground, D5=>OrResult(7), D6=>AddResult(7), D7=>AndResult(7),
            E=>Power, S0=>Op(0), S1=>Op(1), S2=>Op(2), O=>Result_DUMMY(7));
   
   XLXI_37 : GND
      port map (G=>Ground);
   
   XLXI_38 : VCC
      port map (P=>Power);
   
   XLXI_39 : XOR2
      port map (I0=>OverFlow, I1=>AddResult(7), O=>XLXN_137);
   
   XLXI_40 : NOR8_MXILINX_eightbitalu
      port map (I0=>Result_DUMMY(7), I1=>Result_DUMMY(6), I2=>Result_DUMMY(5),
            I3=>Result_DUMMY(4), I4=>Result_DUMMY(3), I5=>Result_DUMMY(2),
            I6=>Result_DUMMY(1), I7=>Result_DUMMY(0), O=>ZBit);
   
end BEHAVIORAL;


