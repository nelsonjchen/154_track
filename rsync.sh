#!/bin/sh

# rsync -avz -e ssh nelsonchen@csil.cs.ucsb.edu:~nelsonchen/public_html/ test/
rsync -avz -e ssh nelsonchen@csil.cs.ucsb.edu:~franklin/public_html/154/ 154/ --exclude='*.swp' --exclude-from=154.lst --delete-excluded
rsync -avz --exclude-from=bin.lst -e ssh nelsonchen@csil.cs.ucsb.edu:~franklin/bin/ bin/
rsync -avz -e ssh nelsonchen@csil.cs.ucsb.edu:~franklin/labfiles/154/ 154_labfiles/ --exclude='*.swp' --exclude-from=154.lst --delete-excluded
git add -A
git commit --all -m "update from rsync.sh via `hostname`"
git pull
git push
